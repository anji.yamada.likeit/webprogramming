<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>user detail</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css"
		rel="stylesheet"
		integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl"
		crossorigin="anonymous">
</head>
<body>
<div class="container" style="text-align: center;">
  <h1>ユーザー情報詳細参照</h1>
  <br>


  <table class="table table-hover">
  <thead>
  <tr>
  <th>ログインID</th>
  <th>ユーザー名</th>
  <th>生年月日</th>
  <th>登録日時</th>
  <th>更新日時</th>
  </tr>
  </thead>
  <tbody>
  <tr>
  <td>${userDetail.loginId }</td>
  <td>${userDetail.name }</td>
  <td>${userDetail.birthDate }</td>
  <td>${userDetail.createDate }</td>
  <td>${userDetail.updateDate }</td>
  </tr>
  </tbody>
  </table>
  <br><a href="ListServlet">戻る</a>
</div>
</body>
</html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>sign up</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css"
		rel="stylesheet"
		integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl"
		crossorigin="anonymous">
</head>
<body>
<div class="container">
  <p>${user.name }さん</p>
  <br>
  <a href="LogoutServlet">ログアウト</a><br>
  <br>
  <h1>ユーザー新規登録</h1>
  <p style="color: #ff3300;">${errorMessage }</p>
  <form action="SignUpServlet" method="post">
    <div class="mb-3">
      <label for="exampleInputEmail1" class="form-label">ログインID</label>
      <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
      name="loginId" value="<c:if test="${loginId != empty var }">${loginId }</c:if>">
    </div>
    <br>
    <div class="mb-3">
      <label for="exampleInputEmail1" class="form-label">パスワード</label>
      <input type="password" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="password">
    </div>
    <br>
    <div class="mb-3">
      <label for="exampleInputEmail1" class="form-label">パスワード確認</label>
      <input type="password" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="passwordConfirmation">
    </div>
    <br>
    <div class="mb-3">
      <label for="exampleInputEmail1" class="form-label">ユーザー名</label>
      <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
      name="userName" value="<c:if test="${userName != empty var }">${userName }</c:if>">
    </div>
    <br>
    <div class="mb-3">
      <label for="exampleInputEmail1" class="form-label">生年月日</label>
      <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
      name="birthDate" value="<c:if test="${birthDate != empty var }">${birthDate }</c:if>">
    </div>
    <br>

    <div class="d-grid gap-2 col-6 mx-auto">
  <button class="btn btn-primary" type="submit">登録</button>
</div>
  </form>

  <br>
  <a href="ListServlet">戻る</a>
  </div>
</body>
</html>
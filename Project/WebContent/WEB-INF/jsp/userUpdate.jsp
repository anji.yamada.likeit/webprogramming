<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>userUpdate</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css"
		rel="stylesheet"
		integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl"
		crossorigin="anonymous">
  </head>
  <body>
  <div class="container">
    <p>${user.name }さん</p>
    <br><a href="LogoutServlet">ログアウト</a><br>
    <br><h1>ユーザー情報更新</h1>
    <p style="color: #ff3300;">${errorMessage }</p>

    <p>ログインID : ${userInfo.loginId }</p>
    <br>
    <form action="UpdateServlet" method="post">
      <input type="hidden" value="${userInfo.id}" name="id">
      <div class="mb-3">
        <label for="password" class="form-label">パスワード</label>
        <input type="password" class="form-control" id="exampleInputPassword1" name="password">
      </div>
      <div class="mb-3">
        <label for="password-confirmation" class="form-label">パスワード（確認）</label>
        <input type="password" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="passwordConfirmation">
      </div>
      <div class="mb-3">
        <label for="" class="form-label">ユーザー名</label>
        <input type="text" class="form-control"  aria-describedby="emailHelp" name="userName" value="${userInfo.name }">
      </div>
      <div class="mb-3">
        <label for="" class="form-label">生年月日</label>
        <input type="text" class="form-control"  aria-describedby="emailHelp" name="birthDate" value="${userInfo.birthDate }">
      </div>

      <div class="d-grid gap-2 col-6 mx-auto">
  		<button class="btn btn-primary" type="submit">更新</button>
	  </div>
    </form>

    <br><a href="ListServlet">戻る</a>
    </div>
  </body>
</html>
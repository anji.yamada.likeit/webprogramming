<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Delete</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css"
		rel="stylesheet"
		integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl"
		crossorigin="anonymous">
  </head>
  <body>
  <div class="container">
  <br><p>${user.name }さん</p>
  <div class="content" style="text-align: center;">
    <br><a href="LogoutServlet">ログアウト</a><br>
    <br><h1>ユーザー削除確認</h1><br>
    <h5>ログインID: ${loginId.loginId }<br>を本当に削除してよろしいでしょうか？</h5>

    <form action="DeleteServlet" method="post">
    <input type="hidden" value="${loginId.id }" name="id">
   	<button type="submit" class="btn btn-primary btn-lg" value="delete" name="delete">削除する</button>
   	<button type="submit" class="btn btn-secondary btn-lg" name="cancel">キャンセル</button>
    </form>
    </div>
    </div>
  </body>
</html>
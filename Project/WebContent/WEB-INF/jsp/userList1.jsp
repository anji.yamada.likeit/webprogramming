<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>userList</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
	integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
     <link href="userList.css" rel="stylesheet" type="text/css" />
</head>
<body>
<%--管理者以外がログインした場合のjsp --%>

	<p>${user.name }さん</p>

  <br><a href = "LogoutServlet">ログアウト</a><br>

  <br><a href="SignUpServlet">新規登録</a><br>


	<br><h1>ユーザ一覧</h1><br>

	<form action="ListServlet" method="post">
    <div class="mb-3">
      <label for="exampleInputEmail1" class="form-label">ログインID</label>
      <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="loginId">
      </div>
    <div class="mb-3">
      <label for="exampleInputPassword1" class="form-label">ユーザー名</label>
      <input type="text" class="form-control" id="exampleInputPassword1" name="userName">
    </div>
      <p>生年月日</p>
      <input type = "date" name="date1">
      <p>~</p>
      <input type = "date" name="date2">
      <button type="submit" class="btn btn-primary">検索</button>
  </form>

  <br>
  <div style="display: flex;">
  	<p style="margin-right: 100px;">ログインID</p>
  	<p style="margin-right: 100px;">ユーザ名</p>
  	<p style="margin-right: 100px;">生年月日</p>
  </div>

		<c:forEach var="user" items="${userList }">
			<div>
		    <td>${user.loginId }</td>
		    <td>${user.name }</td>
		    <td>${user.birthDate }</td>
		    <td><a href="DetailServlet?id=${user.id }">詳細</a></td>
		    <c:if test="${user.loginId == sessionScope.user.loginId }">
		    <a href="UpdateServlet?id=${user.id }">更新</a>
		    </c:if>
		    </div>
    	</c:forEach>
</body>
</html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" 
  integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
  <title>login</title>
</head>
<body>
<div class="container">
  <h1>ログイン画面</h1>
   <p style="color: #ff3300;">${errorMessage}</p>
  <form action="LoginServlet" method="post">
      <label class="form-label">ログインID</label>
      <input type="text" name="loginId">
      <label for="exampleInputPassword1" class="form-label">パスワード</label>
      <input type="password" name="password">
    <button type="submit" class="btn btn-primary">ログイン</button>
  </form>
</div>
</body>
</html>
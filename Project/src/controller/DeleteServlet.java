package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class DeleteServlet
 */
@WebServlet("/DeleteServlet")
public class DeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		User userSession = (User)session.getAttribute("user");
		
		if (userSession == null) {
			response.sendRedirect("LoginServlet");
		} else {
			//ユーザーを紐ずけるIDを取得
			String id = request.getParameter("id");

			//DAOのメソッド呼び出し
			UserDao userDao = new UserDao();
			User user = userDao.findUserInfo1(id);

			//ユーザー情報をリクエストスコープにセット
			request.setAttribute("loginId", user);

			//jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/delete.jsp");
			dispatcher.forward(request, response);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//jspからリクエストパラメーターを取得する
		String delete = request.getParameter("delete");
		String id = request.getParameter("id");

		//空の変数に対してequalsを使うとnullPointerExceptionが発生する
		if (delete != null) {
			UserDao userDao = new UserDao();
			int result = userDao.delete(id);
			response.sendRedirect("ListServlet");
		} else {
			response.sendRedirect("ListServlet");
		}

	}

}

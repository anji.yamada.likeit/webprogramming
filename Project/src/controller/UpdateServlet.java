package controller;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdate
 */
@WebServlet("/UpdateServlet")
public class UpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		User user = (User)session.getAttribute("user");

		//検索結果から遷移してきた場合の処理
		String userId = request.getParameter("userId");
		if (userId != null) {
			UserDao userDao = new UserDao();
			User userInfo = userDao.findUserInfo(userId);

			request.setAttribute("userInfo", userInfo);

			//userUpdate.jspにフォワードする
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
		}

		//ログイン情報がない場合は、ログイン画面に遷移する
		if (user == null) {
			response.sendRedirect("LoginServlet");
		} else {
			//ユーザーを紐付けるためのIDをリクエストパラメーターで取得
			String id = request.getParameter("id");

			UserDao userDao = new UserDao();
			User userInfo = userDao.findUserInfo(id);

			request.setAttribute("userInfo", userInfo);

			//userUpdate.jspにフォワードする
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//文字化け防止の記述
		request.setCharacterEncoding("UTF-8");

		//ユーザーを紐付けるためのID
		String id = request.getParameter("id");

		//リクエストパラメーター取得
		String password = request.getParameter("password");
		String passwordConfirmation = request.getParameter("passwordConfirmation");
		String userName = request.getParameter("userName");
		String birthDate = request.getParameter("birthDate");

		if (!password.equals(passwordConfirmation) || userName.equals("") || birthDate.equals("")) {
			request.setAttribute("errorMessage", "入力された内容は正しくありません。");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);

		} else if (password.equals("") && passwordConfirmation.equals("")) {
			UserDao userDao = new UserDao();
			int result = userDao.userUpdate1(userName, birthDate, id);

			if (result == 0) {
				request.setAttribute("errorMessage", "入力された内容は正しくありません。");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
				dispatcher.forward(request, response);
			} else {
				response.sendRedirect("ListServlet");
			}
		} else {
			//ハッシュを生成したい元の文字列
			String source = password;
			//ハッシュ生成前にバイト配列に置き換える際のCharset
			Charset charset = StandardCharsets.UTF_8;
			//ハッシュアルゴリズム
			String algorithm = "MD5";

			//ハッシュ生成処理
			byte[] bytes = null;
			try {
				bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
			} catch (NoSuchAlgorithmException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}
			String encryptPassword = DatatypeConverter.printHexBinary(bytes);

			UserDao userDao = new UserDao();
			int resultDao = userDao.userUpdate(userName, encryptPassword, birthDate, id);

			if (resultDao == 0) {
				request.setAttribute("errorMessage", "入力された内容は正しくありません。");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
				dispatcher.forward(request, response);
			} else {
				response.sendRedirect("ListServlet");
			}
		}

	}

}

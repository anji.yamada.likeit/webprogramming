package controller;


import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserListServlet
 */
@WebServlet("/ListServlet")
public class ListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//セッションにログイン情報がない場合、ログイン画面に遷移する
		HttpSession session = request.getSession();
		User user = (User)session.getAttribute("user");

		if (user != null) {
			//Daoのメソッドを呼び出して全てのユーザー情報を取得
			UserDao userDao = new UserDao();
			List<User> userList = userDao.findAll();
			request.setAttribute("userList", userList);
			
			if (user.getName().equals("管理者")) {
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
				dispatcher.forward(request, response);
			} else {
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList1.jsp");
				dispatcher.forward(request, response);
			}
		} else {
			response.sendRedirect("LoginServlet");
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		//ログインしている人のセッション情報を取得
		HttpSession session = request.getSession();
		User user = (User)session.getAttribute("user");
		
		//リクエストパラメーターを取得
		String loginId = request.getParameter("loginId");
		String userName = request.getParameter("userName");
		String date1 = request.getParameter("date1");
		String date2 = request.getParameter("date2");
		
		UserDao userDao = new UserDao();
		List<User> userList = userDao.search(loginId, userName, date1, date2);
		
		if (userList != null) {
			//管理者以外の処理
			if (!user.getName().equals("管理者")) {
				request.setAttribute("userList", userList);
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList1.jsp");
				dispatcher.forward(request, response);
			}
			
			request.setAttribute("userList", userList);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
			dispatcher.forward(request, response);
		} else {
			request.setAttribute("erm", "該当するデータがありません。");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
			dispatcher.forward(request, response);
		}
		

	}
	
}

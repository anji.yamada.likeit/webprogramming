package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.User;

public class UserDao {
	public User findByLoginInfo(String loginId, String password) {
        Connection conn = null;

        try {
        	//DB接続
        	conn = DBManager.getConnection();

        	//sql分準備
        	String sql = "select * from user where login_id = ? and password = ?";

        	PreparedStatement pStmt = conn.prepareStatement(sql);
        	pStmt.setString(1, loginId);
        	pStmt.setString(2, password);
        	ResultSet rs = pStmt.executeQuery();

        	//サーブレットでエラーメッセージを表示させたいからある記述。表示させない場合はいらない。
        	if (!rs.next()) {
        		return null;
        	}

        	String loginIdData = rs.getString("login_id");
        	String nameData = rs.getString("name");
        	return new User(loginIdData, nameData);




        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }

	}

	public List<User> findAll() {

        Connection conn = null;
        List<User> userList = new ArrayList<User>();


        try {
        	conn = DBManager.getConnection();

        	String sql = "select * from user where id <> 1";

        	Statement stmt = conn.createStatement();
        	ResultSet rs = stmt.executeQuery(sql);

        	while (rs.next()) {
        		int id = rs.getInt("id");
        		String loginId = rs.getString("login_id");
        		String name = rs.getString("name");
        		Date birthDate = rs.getDate("birth_date");
        		String password = rs.getString("password");
        		String createDate = rs.getString("create_date");
        		String updateDate = rs.getString("update_date");

        		User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);
        		userList.add(user);
        	}

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return userList;
	}

	public int signUp(String loginId, String password, String userName, String birthDate) {
		Connection conn = null;

		try {
			//DB接続
			conn = DBManager.getConnection();

			//SQL文準備
			String sql = "insert into user (login_id, name, birth_date, password, create_date, update_date) "
					+ "values (?, ?, ?, ?, now(), now())";

			//SQL実行
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, userName);
			pStmt.setString(3, birthDate);
			pStmt.setString(4, password);

			int result = pStmt.executeUpdate();

			if (result < 1) {
				return 0;
			}

			return result;
		} catch (SQLException e) {
            e.printStackTrace();
            return 0;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        }

	}

	public User findUserDetail(String id) {
		Connection conn =null;

		try {
			conn = DBManager.getConnection();

			String sql = "select login_id, name, birth_date, create_date, update_date from user where id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");

			User user = new User(loginId, name, birthDate, createDate, updateDate);
			return user;

		} catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
	}

	public int userUpdate(String userName, String password, String birthDate, String id) {
		Connection conn = null;

		try {
			conn = DBManager.getConnection();

			String sql = "update user set name = ?, password = ?, birth_date = ? where id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, userName);
			pStmt.setString(2, password);
			pStmt.setString(3, birthDate);
			pStmt.setString(4, id);
			int result = pStmt.executeUpdate();

			if (result < 1) {
				return 0;
			}

			return result;

		} catch (SQLException e) {
            e.printStackTrace();
            return 0;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        }
	}

	public int userUpdate1(String userName, String birthDate, String id) {

		Connection conn = null;

		try {
			conn = DBManager.getConnection();

			String sql = "update user set name = ?, birth_date = ? where id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, userName);
			pStmt.setString(2, birthDate);
			pStmt.setString(3, id);
			int result = pStmt.executeUpdate();

			if (result < 1) {
				return 0;
			}

			return result;

		} catch (SQLException e) {
            e.printStackTrace();
            return 0;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        }

	}

	public User findUserInfo(String id) {
		Connection conn = null;

		try {
			conn = DBManager.getConnection();

			String sql = "select id, login_id, name, birth_date from user where id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int userId = rs.getInt("id");
			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			User user = new User(userId, loginId, name, birthDate);

			return user;

		} catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
	}

	public User findUserInfo1(String id) {

		Connection conn = null;

		try {
			conn = DBManager.getConnection();

			String sql = "select login_id, id from user where id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int userId = rs.getInt("id");
			String loginId = rs.getString("login_id");
			return new User(userId, loginId);

		} catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }

	}

	public int delete(String id) {


		Connection conn = null;

		try {
			conn = DBManager.getConnection();

			String sql = "delete from user where id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			int result = pStmt.executeUpdate();

			return result;

		} catch (SQLException e) {
            e.printStackTrace();
            return 0;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        }

	}

	public List<User> search(String loginId, String userName, String date1, String date2) {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

        try {
        	conn = DBManager.getConnection();

        	//管理者は出力しないsql
        	String sql = "select * from user where id <> 1";

        	//一つでも入力されたら検索結果が出るようにする処理(メンター質問箇所)
        	if(!loginId.equals("")) {
        		//andの前にスペースを入れないとスペースが入っていないsql文になってしまってエラーが出る
        		sql += " and login_id = '" + loginId + "'";
        	}

        	if (!userName.equals("")) {
        		sql += " and name like '%" + userName + "%'";
        	}

        	if (!date1.equals("")) {
        		sql += " and birth_date >= '" + date1 + "'";
        	}

        	if (!date2.equals("")) {
        		sql += " and birth_date <= '" + date2 + "'";
        	}

        	Statement stmt = conn.createStatement();
        	ResultSet rs = stmt.executeQuery(sql);

        	while (rs.next()) {
        		String userLoginId = rs.getString("login_id");
        		String name = rs.getString("name");
        		Date birthDate = rs.getDate("birth_date");
        		int id = rs.getInt("id");
        		User user = new User(userLoginId, name, birthDate, id);
        		userList.add(user);
        	}

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return userList;
	}

}